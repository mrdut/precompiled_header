#################
# CCACHE CONFIG #
#################
# install ccache : sudo apt-get install ccache
# configure cache : .zshrc : export PATH="/usr/lib/ccache:$PATH"
# check : which gcc should be : /usr/lib/ccache/gcc
# export : export CCACHE_SLOPPINESS=pch_defines,time_macros
# cmake : add_compile_options(-fpch-preprocess)


run(){
    echo " -- run $CCACHE_RUN_COUNT -- "

    # make bulid dir if it does not exists
    mkdir -pv build

    # go to build directory
    cd build/

    # rm all files but precompiled header
    ls | grep -v *.gch | xargs rm -rf

    # setup ccache log file
    export CCACHE_LOGFILE=/tmp/ccache_log${CCACHE_RUN_COUNT}.txt
    rm -f $CCACHE_LOGFILE

    # build all
    cmake $CMAKE_OPTION .. > $LOG_STDOUT || exit -1
    make $MAKE_VERBOSE > $LOG_STDOUT || exit -1
    ./bin/prog > $LOG_STDOUT || exit -1

    # print ccache statistics
    echo "cache log file                      $CCACHE_LOGFILE"
    ccache -s

    # go to build directory
    cd ..

    # increase run count
    export CCACHE_RUN_COUNT=$((CCACHE_RUN_COUNT+1))

    echo ""
}

# setup temporary ccache working directory
export CCACHE_DIR="/tmp/ccache_tmp"

# options required by ccache to use precompiled header
export CCACHE_SLOPPINESS=pch_defines,time_macros

# init ccache run count variable (for log files naming in run function)
export CCACHE_RUN_COUNT=0

# uncomment this to make compilation without precompiled header
# export CMAKE_OPTION="-DUSE_PRECOMPILED_HEADER=OFF"

# uncomment this to make verbose build
# export MAKE_VERBOSE="VERBOSE=1"

# uncomment second line to make silent build
export LOG_STDOUT=/dev/stdout
export LOG_STDOUT=/dev/null

echo "-- init --"
ccache -cCz # clear all !!
echo ""

if [ "$#" -eq 0 ]; then
    run
else
    for (( c=1; c<=$1; c++ ))
    do
       run
    done
fi
