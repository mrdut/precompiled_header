#ifndef LIBRARY_FILE_1_HPP
#define LIBRARY_FILE_1_HPP

      #ifndef USE_PRECOMPILED
  		  #define txt "classic text"
      #else
        #define txt "precompiled text"
      #endif

    void libfunc1();

#endif