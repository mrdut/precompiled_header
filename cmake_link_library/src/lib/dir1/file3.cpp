// #ifdef USE_PRECOMPILED
//     #include "precompiled_header.hpp"
// #else
//     #include <iostream>
// #endif

#include "lib/dir1/file3.hpp"
#include "lib/dir1/file1.hpp"


void libfunc3()
{
    libfunc1();

    std::cout << " from function 3" << std::endl;
    // std::cout << txt << std::endl;
}
