// #ifdef USE_PRECOMPILED
//     #include "precompiled_header.hpp"
// #else
//     #include <iostream>
// #endif


#include "lib/dir1/file1.hpp"
#include "lib/dir1/file3.hpp"
#include "lib/dir2/file2.hpp"

int main(int argc, char** argv)
{
    std::cout << "\n -= " << argv[0] << " =-\n";
    // std::cout << "(main) " << txt <<std::endl;
    std::cout << "(func1) ";
    libfunc1();
    std::cout << "(func2) ";
    libfunc2();
    std::cout << "(func3) ";
	libfunc3();
    std::cout << std::endl;
    return 0;
}