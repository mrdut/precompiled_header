#ifndef LIBRARY_FILE_HPP
#define LIBRARY_FILE_HPP

#ifndef USE_PRECOMPILED
    #include "header.hpp"
    #define txt "I'm using classic header"
#else
    #include "precompiled_header.hpp"
    #define txt "I'm using precompiled header"
#endif

void libfunc();

#endif