
# clean workspace
rm build -rf

# create build directories
mkdir build build/bin build/precompiled_header

# build targets
make build/precompiled_header/precompiled_header.hpp.gch
make build/bin/Hey
make build/bin/PrecompiledHey

# run targets
printf "\n*******\n* run *\n*******\n"
./build/bin/Hey
./build/bin/PrecompiledHey
