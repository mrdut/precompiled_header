#ifndef USE_PRECOMPILED
    #include "header.hpp"
    #define txt "I'm using classic header"
#else
    #include "precompiled_header.hpp"
    #define txt "I'm using precompiled header"
#endif

int main(int argc, char** argv)
{
    std::cout << txt << std::endl;
    return 0;
}