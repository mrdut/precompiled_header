cmake_minimum_required(VERSION 2.8.11)

project('PRECOMPILED' CXX)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY bin/)
# set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
# set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

set(classic_executable Hey)
set(precompiled_executable PrecompiledHey)

###########
# classic #
###########
add_executable(${classic_executable} src/main.cpp)

###############
# precompiled #
###############
add_executable(${precompiled_executable} src/main.cpp)
target_compile_definitions(${precompiled_executable} PUBLIC -DUSE_PRECOMPILED )

set(precompiled_target_name precompiled_header )
set(precompiled_header_src src/header.hpp )
set(precompiled_header_dst precompiled_header.hpp.gch )
add_custom_target( ${precompiled_target_name} ALL DEPENDS ${precompiled_header_dst} )
add_custom_command( OUTPUT ${precompiled_header_dst}
                        COMMAND ${CMAKE_CXX_COMPILER}  -DUSE_PRECOMPILED ${CMAKE_CURRENT_SOURCE_DIR}/${precompiled_header_src} -o ${precompiled_header_dst}
                        MAIN_DEPENDENCY ${CMAKE_CURRENT_SOURCE_DIR}/${precompiled_header_src}
                        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                        VERBATIM )
add_dependencies( ${precompiled_executable} ${precompiled_target_name} )

target_include_directories(${precompiled_executable} PUBLIC ${CMAKE_CURRENT_BINARY_DIR})